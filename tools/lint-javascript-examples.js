const fs = require('fs');
const path = require('path');
const EsLintCLIEngine = require('eslint').CLIEngine;
const EXTRACT_JAVASCRIPT_EXAMPLE = /```javascript([^]+?)```/gi;
const JAVASCRIPT_MD_PATH = path.join(__dirname, '../javascript.md');
const ESLINTRC_PATH = path.join(__dirname, '../.eslintrc');
const eslint = new EsLintCLIEngine({
    configFile: ESLINTRC_PATH
});

const SEVERITY = {
    "0": "",
    "1": "WARNING",
    "2": "ERROR"
};

function extractRegExpResultLineNumber(startIndex, endIndex, text) {
    return text.substring(startIndex, endIndex).match(/\n/g).length;
}

function formatEsLintMessage(message, evaluatedScriptLineNumber) {
    return [
        message.line + evaluatedScriptLineNumber,
        SEVERITY[message.severity],
        message.ruleId,
        message.message
    ].join(' ');
}

function readFile(filePath, callback) {
    fs.readFile(filePath, 'UTF-8', function (error, data) {
        if (error) {
            console.log('Oops', error);
            process.exit(1);
        } else {
            try {
                callback.call(null, data);
            } catch (error) {
                console.log(error.stack);
            }
        }
    });
}

readFile(JAVASCRIPT_MD_PATH, function (data) {
    var regExpResult;
    var evaluatedScriptLineNumber = 0;
    var lastRegExpResultIndex = 0;
    while (regExpResult = EXTRACT_JAVASCRIPT_EXAMPLE.exec(data)) {
        evaluatedScriptLineNumber = extractRegExpResultLineNumber(lastRegExpResultIndex, regExpResult.index, data) + evaluatedScriptLineNumber;
        lastRegExpResultIndex = regExpResult.index;
        var javascriptExample = regExpResult[1];
        var messages = eslint.executeOnText(javascriptExample, 'w00t!').results[0].messages;
        for (var i = 0, count = messages.length; i < count; i++) {
            console.log(formatEsLintMessage(messages[i], evaluatedScriptLineNumber));
        }
    }
});


