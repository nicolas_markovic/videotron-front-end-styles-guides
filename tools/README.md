# Tools

This folder contain tools related to this style guide.

## eslint-javascript-stule-guide.js

This tool lint the examples contained in the file `javascript.md`. This tool use the rules defined in `.eslintrc`.

- `cd ./style-guide`
- `node ./tools/lint-javascript-examples.js`


